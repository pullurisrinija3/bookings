package com.greatlearning.capstone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class RentAPlaceEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RentAPlaceEurekaServerApplication.class, args);
		System.err.println("server running on port number 8761");
	}
}
