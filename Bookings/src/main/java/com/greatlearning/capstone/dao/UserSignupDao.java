package com.greatlearning.capstone.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.capstone.bean.UserSignup;

@Repository
public interface UserSignupDao extends JpaRepository<UserSignup,String> {

}
