package com.greatlearning.capstone.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.capstone.bean.OwnerLogin;

@Repository
public interface OwnerLoginDao extends JpaRepository<OwnerLogin,String> {

}
