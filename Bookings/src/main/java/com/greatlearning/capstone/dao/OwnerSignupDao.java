package com.greatlearning.capstone.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.capstone.bean.OwnerSignup;

@Repository
public interface OwnerSignupDao extends JpaRepository<OwnerSignup,String> {

}
