package com.greatlearning.capstone.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.capstone.bean.UserLogin;
import com.greatlearning.capstone.bean.UserSignup;

@Repository
public interface UserLoginDao extends JpaRepository<UserLogin,String>{

}
