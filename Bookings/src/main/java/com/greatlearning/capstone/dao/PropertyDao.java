package com.greatlearning.capstone.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.capstone.bean.Property;

@Repository
public interface PropertyDao extends JpaRepository<Property,String> {
	List<Property> findAll();
	List<Property> findByLocation(String location);
}
