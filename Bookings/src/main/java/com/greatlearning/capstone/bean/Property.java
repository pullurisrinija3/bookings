package com.greatlearning.capstone.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Property {
	@Id
	private int propertyId;
	private String propertyName;
	private String propertyType;
	private String location;
	private String noOfGuest;
	private int rooms;
	private int beds;
	private double roomSize;
	private String moreInfo;
	private int price;
	private float ratings;
	private String image1;
	private String image2;
	private String image3;
	private String image4;
	private String image5;
	public int getPropertyId() {
		return propertyId;
	}
	public void setPropertyId(int propertyId) {
		this.propertyId = propertyId;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getPropertyType() {
		return propertyType;
	}
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getNoOfGuest() {
		return noOfGuest;
	}
	public void setNoOfGuest(String noOfGuest) {
		this.noOfGuest = noOfGuest;
	}
	public int getRooms() {
		return rooms;
	}
	public void setRooms(int rooms) {
		this.rooms = rooms;
	}
	public int getBeds() {
		return beds;
	}
	public void setBeds(int beds) {
		this.beds = beds;
	}
	public double getRoomSize() {
		return roomSize;
	}
	public void setRoomSize(double roomSize) {
		this.roomSize = roomSize;
	}
	public String getMoreInfo() {
		return moreInfo;
	}
	public void setMoreInfo(String moreInfo) {
		this.moreInfo = moreInfo;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public float getRatings() {
		return ratings;
	}
	public void setRatings(float ratings) {
		this.ratings = ratings;
	}
	public String getImage1() {
		return image1;
	}
	public void setImage1(String image1) {
		this.image1 = image1;
	}
	public String getImage2() {
		return image2;
	}
	public void setImage2(String image2) {
		this.image2 = image2;
	}
	public String getImage3() {
		return image3;
	}
	public void setImage3(String image3) {
		this.image3 = image3;
	}
	public String getImage4() {
		return image4;
	}
	public void setImage4(String image4) {
		this.image4 = image4;
	}
	public String getImage5() {
		return image5;
	}
	public void setImage5(String image5) {
		this.image5 = image5;
	}
	public Property(int propertyId, String propertyName, String propertyType, String location, String noOfGuest,
			int rooms, int beds, double roomSize, String moreInfo, int price, float ratings, String image1,
			String image2, String image3, String image4, String image5) {
		super();
		this.propertyId = propertyId;
		this.propertyName = propertyName;
		this.propertyType = propertyType;
		this.location = location;
		this.noOfGuest = noOfGuest;
		this.rooms = rooms;
		this.beds = beds;
		this.roomSize = roomSize;
		this.moreInfo = moreInfo;
		this.price = price;
		this.ratings = ratings;
		this.image1 = image1;
		this.image2 = image2;
		this.image3 = image3;
		this.image4 = image4;
		this.image5 = image5;
	}
	public Property() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Property [propertyId=" + propertyId + ", propertyName=" + propertyName + ", propertyType="
				+ propertyType + ", location=" + location + ", noOfGuest=" + noOfGuest + ", rooms=" + rooms + ", beds="
				+ beds + ", roomSize=" + roomSize + ", moreInfo=" + moreInfo + ", price=" + price + ", ratings="
				+ ratings + ", image1=" + image1 + ", image2=" + image2 + ", image3=" + image3 + ", image4=" + image4
				+ ", image5=" + image5 + "]";
	}


}
