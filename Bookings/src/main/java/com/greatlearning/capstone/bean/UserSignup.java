package com.greatlearning.capstone.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserSignup {
@Id
private int userId;
private String firstname;
private String lastname;
private String email;
private String phone;
private String location;
private String password;
public int getUserId() {
	return userId;
}
public void setUserId(int userId) {
	this.userId = userId;
}
public String getFirstname() {
	return firstname;
}
public void setFirstname(String firstname) {
	this.firstname = firstname;
}
public String getLastname() {
	return lastname;
}
public void setLastname(String lastname) {
	this.lastname = lastname;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public String getLocation() {
	return location;
}
public void setLocation(String location) {
	this.location = location;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public UserSignup(int userId, String firstname, String lastname, String email, String phone, String location,
		String password) {
	super();
	this.userId = userId;
	this.firstname = firstname;
	this.lastname = lastname;
	this.email = email;
	this.phone = phone;
	this.location = location;
	this.password = password;
}
public UserSignup() {
	super();
	// TODO Auto-generated constructor stub
}
@Override
public String toString() {
	return "UserSignup [userId=" + userId + ", firstname=" + firstname + ", lastname=" + lastname + ", email=" + email
			+ ", phone=" + phone + ", location=" + location + ", password=" + password + "]";
}

}
