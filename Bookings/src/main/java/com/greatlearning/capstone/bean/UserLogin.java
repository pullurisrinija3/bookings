package com.greatlearning.capstone.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserLogin {
@Id
private String name;
private String emailId;
private String phoneNo;
private String password;
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public String getPhoneNo() {
	return phoneNo;
}
public void setPhoneNo(String phoneNo) {
	this.phoneNo = phoneNo;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public UserLogin(String name, String emailId, String phoneNo, String password) {
	super();
	this.name = name;
	this.emailId = emailId;
	this.phoneNo = phoneNo;
	this.password = password;
}
public UserLogin() {
	super();
	// TODO Auto-generated constructor stub
}
@Override
public String toString() {
	return "UserLogin [name=" + name + ", emailId=" + emailId + ", phoneNo=" + phoneNo + ", password=" + password + "]";
}

}
