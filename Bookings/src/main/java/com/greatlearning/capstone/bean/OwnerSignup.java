package com.greatlearning.capstone.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class OwnerSignup {
@Id
private int ownerId;
private String name;
private String email;
private String phone;
private String location;
private String password;
public int getOwnerId() {
	return ownerId;
}
public void setOwnerId(int ownerId) {
	this.ownerId = ownerId;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public String getLocation() {
	return location;
}
public void setLocation(String location) {
	this.location = location;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public OwnerSignup(int ownerId, String name, String email, String phone, String location, String password) {
	super();
	this.ownerId = ownerId;
	this.name = name;
	this.email = email;
	this.phone = phone;
	this.location = location;
	this.password = password;
}
public OwnerSignup() {
	super();
	// TODO Auto-generated constructor stub
}
@Override
public String toString() {
	return "OwnerSignup [ownerId=" + ownerId + ", name=" + name + ", email=" + email + ", phone=" + phone
			+ ", location=" + location + ", password=" + password + "]";
}


}
