package com.greatlearning.capstone.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.greatlearning.capstone.bean.OwnerLogin;
import com.greatlearning.capstone.bean.OwnerSignup;
import com.greatlearning.capstone.dao.OwnerLoginDao;
import com.greatlearning.capstone.dao.OwnerSignupDao;

@Service
public class OwnerService {
	@Autowired
	OwnerLoginDao ownerlogindao;
	public List<OwnerLogin> getAllOwnerDetails(){
		List<OwnerLogin> ownerlogin=ownerlogindao.findAll();
		return ownerlogin;
	}
	public String verifyPassword(OwnerLogin owner) {
		if(ownerlogindao.existsById(owner.getEmailId())){
			OwnerLogin ownerlogin=ownerlogindao.getById(owner.getEmailId());
		if(ownerlogin.getPassword().equals(owner.getPassword())) {
			return " owner login successfull";
		}
		else {
			return "Wrong password...!!";
		}
		}else {
			return "Details not found";
		}
		}
	//Owner Signup
	
	@Autowired
	OwnerSignupDao ownersignupdao;
	
	public String createAccount(OwnerSignup owner) {
		if(ownersignupdao.existsById(owner.getEmail())) {
			return "OwnerDetails already present, please login";
		}else {
			ownersignupdao.save(owner);
			return "owner account created successfully";
		}
	}
		
	}
