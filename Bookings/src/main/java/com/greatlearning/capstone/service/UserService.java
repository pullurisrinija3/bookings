package com.greatlearning.capstone.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.greatlearning.capstone.bean.UserLogin;
import com.greatlearning.capstone.bean.UserSignup;
import com.greatlearning.capstone.dao.UserLoginDao;
import com.greatlearning.capstone.dao.UserSignupDao;

@Service
public class UserService {
	@Autowired
	UserLoginDao userlogindao;
	public List<UserLogin> getAllUserDetails(){
		List<UserLogin> userlogin=userlogindao.findAll();
		return userlogin;
	}
	public String verifyPassword(UserLogin user) {
		if(userlogindao.existsById(user.getEmailId())){
			UserLogin userlogin=userlogindao.getById(user.getEmailId());
		if(userlogin.getPassword().equals(user.getPassword())) {
			return " user login successfull";
		}
		else {
			return "Wrong password...!!";
		}
		}else {
			return "Details not found";
		}
		}
	//User Signup
	
	@Autowired
	UserSignupDao usersignupdao;
	
	public String createAccount(UserSignup user) {
		if(usersignupdao.existsById(user.getEmail())) {
			return "UserDetails already present, please login";
		}else {
			usersignupdao.save(user);
			return "user account created successfully";
		}
	}

}
