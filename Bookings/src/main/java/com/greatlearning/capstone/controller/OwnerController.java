package com.greatlearning.capstone.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.capstone.bean.OwnerLogin;
import com.greatlearning.capstone.service.OwnerService;

@RestController
@RequestMapping(value="")
public class OwnerController {
	@Autowired
	OwnerService ownerService;
	
	//owner login
	
	@GetMapping(value="")
	public String verifyPassword(@RequestBody OwnerLogin ownerlogin) {
		return ownerService.verifyPassword(ownerlogin);
	}
	@GetMapping(value="getAllOwnerLoginDetails")
	public List<OwnerLogin> getAllOwnerLoginDetails(){
		return ownerService.getAllOwnerDetails();
	}
	
	@GetMapping(value = "adminLogout")
	public String logout(HttpSession hs) {
		hs.invalidate();
		return "index";
	}

	@GetMapping(value = "adminHome")
	public String openAdminHome() {
		return "adminHome";
	}
}
