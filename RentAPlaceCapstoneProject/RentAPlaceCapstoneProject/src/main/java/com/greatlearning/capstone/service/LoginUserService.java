package com.greatlearning.capstone.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.capstone.entity.LoginUser;
import com.greatlearning.capstone.repository.LoginUserRepository;

@Service
public class LoginUserService {
	/*
	 * @annotation => @Autowired -- is used to establish connection between classess.
	 * Here we have established connection to
	 * LoginUserRepository
	 * 
	 */
	@Autowired
	private LoginUserRepository loginUserRepository;
	
	/*
	 * User REGISTER
	 * Methods used to insert new user details into database
	 * @param admin of type LoginUser
	 * @return boolean
	 *  
	 */
	public boolean insertUser(LoginUser loginUser) {
		if(!this.loginUserRepository.existsById(loginUser.getEmail())) {
			this.loginUserRepository.save(loginUser);
			return true;
		}
		return false;
	}
	
	/*
	 * ADMIN REGISTER
	 * Methods used to insert admin details into database
	 * @param admin of type LoginUser
	 * @return boolean
	 *  
	 */
	public boolean insertAdmin(LoginUser loginAdmin) {
		if(!this.loginUserRepository.existsById(loginAdmin.getEmail())) {
			this.loginUserRepository.save(loginAdmin);
			return true;
		}
		return false;
	}
	
}
