package com.greatlearning.capstone.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="userregistration")
public class UserRegistration {

	/*
	 * <h1>The table structure for storing user deails</h1>
	 * 
	 * @see #userId => userId of the user detail in the database
	 * @see #firstname => firstname is the firstname in database
	 * @see #lastname => lastname is the lastname
	 * @see #email  => email is the login email
	 * @see #phone => phone is the phone number of the user
	 * @see #address => address for the address of the user
	 * @see #password => password for the user email
	 * @see #confirm_password => confirm_password for the user confirm_password
	 * 
	 *  @annotation => @Entity -- Tells the spring boot to create the table with name "admin" as class name
	 *  @annotation => @Id -- creates "userId" as a primary key in admin table
	 *  @annotation => @GeneratedValue -- specifies how to generate value for a particular column 
	 *          GenerationType.IDENTITY -- It's similar to AUTO generation type. i.e.., Auto_increment is generated for particular column 
	 * */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="userid")
	private int userId;
	private String firstname;
	private String lastname;
	private String email;
	private String phone;
	private String address;
	private String password;
	private String confirmPassword;

	/*
	 * 
	 * Default constructor of user class
	 * 
	 *  */
	public UserRegistration() {
		// TODO Auto-generated constructor stub
	}

	/*
	 *parameterised constructor of user class
	 * @param args unused
	 * 
	 * */
	public UserRegistration(String firstname, String lastname, String email, String phone, String address, String password,
			String confirmPassword) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.password = password;
		this.confirmPassword = confirmPassword;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	/*
	 * {@link java.lang.class#toString()} method.
	 * @return string representation of this action
	 */
	@Override
	public String toString() {
		return "UserRegistration [userId=" + userId + ", firstname=" + firstname + ", lastname=" + lastname + ", email="
				+ email + ", phone=" + phone + ", address=" + address + ", password=" + password + ", confirmPassword="
				+ confirmPassword + "]";
	}


}
