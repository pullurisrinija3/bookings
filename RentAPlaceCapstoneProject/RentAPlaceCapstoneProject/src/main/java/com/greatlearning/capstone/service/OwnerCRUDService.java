package com.greatlearning.capstone.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.capstone.entity.Owner;
import com.greatlearning.capstone.repository.OwnerCRUDRepository;

@Service
public class OwnerCRUDService {
	@Autowired
	OwnerCRUDRepository ownerDao;
	public String storeOwnerDetails(Owner owner)
	{	
		if(ownerDao.existsById(owner.getEmail())) 
		{
		  return "Email must be unique";
		}else {
		  ownerDao.save(owner);
		  return "owner details stored successfully";
		}
    }
	
	public String deleteOwnerDetails(String Email)
	{
		if(!ownerDao.existsById(Email)) {
			return "No Owner present with the given email";
			}else {
			ownerDao.deleteById(Email);
			return "Owner deatils deleted successfully";
			}	
	}
	
	public String updateOwnerDetails(Owner owner) {
		if(!ownerDao.existsById(owner.getEmail())) 
		{
		    return "No Owner present with the given email";
		}
		else 
		{
			Owner o= ownerDao.getById(owner.getEmail()); 
			o.setPassword(owner.getPassword());					
			ownerDao.saveAndFlush(owner);				
			return "Owner details updated successfully";
		}	
	}
	
	public List<Owner> getAllOwnerDetails(){
		return ownerDao.findAll();
	}
//	public String LogoutDetails(String Email)
//	{
//		if(!ownerDao.existsById(Email)) {
//			return "Owner is not present with the given email";
//			}else {
//			return "Owner logged out successfully";
//			}	
//	}

}
