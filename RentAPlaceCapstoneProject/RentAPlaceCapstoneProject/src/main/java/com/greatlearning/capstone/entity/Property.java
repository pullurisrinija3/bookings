package com.greatlearning.capstone.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Property {
	
	/*
	 * <h1>The table structure for storing Property Data</h1>
	 * 
	 * @see #property_id => property_id for property
	 * @see #property_name => property_name is the property name 
	 * @see #property_type => property_type is the  property type like Appartment,Villa,Resort
	 * @see #location  => location is the property where it is located
	 * @see #max_guest => max_guest is the maximum number of guest in a property
	 * @see #address => address is the address of the property
	 * @see #rooms => rooms is the total number of room in a property
	 * 
	 *  @annotation => @Entity -- Tells the spring boot to create the table with name "property" as class name
	 *  @annotation => @Id -- creates "property_id" as a primary key in property table
	 *  @annotation => @GeneratedValue -- specifies how to generate value for a particular column 
	 *          GenerationType.IDENTITY -- It's similar to AUTO generation type. i.e.., Auto_increment is generated for particular column 
	 * */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int propertyid;
	private String propertyName;
	private String propertyType;
	private String location;
	private String maxGuests;
	private String address;
	private int noOfRooms;
	private double roomSize;
	private String propertyLineContent;
	@Column(length = 500)
	private String moreInfo;
	private int noOfBeds;
	private int price;
	private float ratings;
	private String image1;
	private String image2;
	private String image3;
	private String image4;
	private String image5;

	/*
	 * 
	 * Default constructor of property class
	 * 
	 *  */
	public Property() {
		
	}

	/*
	 *parameterised constructor of the property class
	 * @param args unused
	 * 
	 * */
	public Property(String propertyName, String propertyType, String location, String maxGuests, String address,
			int noOfRooms, double roomSize, String propertyLineContent, String moreInfo, int noOfBeds, int price,
			float ratings, String image1, String image2, String image3, String image4, String image5) {
		super();
		this.propertyName = propertyName;
		this.propertyType = propertyType;
		this.location = location;
		this.maxGuests = maxGuests;
		this.address = address;
		this.noOfRooms = noOfRooms;
		this.roomSize = roomSize;
		this.propertyLineContent = propertyLineContent;
		this.moreInfo = moreInfo;
		this.noOfBeds = noOfBeds;
		this.price = price;
		this.ratings = ratings;
		this.image1 = image1;
		this.image2 = image2;
		this.image3 = image3;
		this.image4 = image4;
		this.image5 = image5;
	}


	/*
	 * @return current property_id of integer type
	 * 
	 * */
	public int getPropertyid() {
		return propertyid;
	}

	/*
	 * @param property id to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setPropertyid(int propertyid) {
		this.propertyid = propertyid;
	}

	/*
	 * @return current property name of string type
	 * 
	 * */
	public String getPropertyName() {
		return propertyName;
	}

	/*
	 * @param property name to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	/*
	 * @return current property_type of string type
	 * 
	 * */
	
	public String getPropertyType() {
		return propertyType;
	}

	/*
	 * @param property type to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	/*
	 * @return current location of string type
	 * 
	 * */
	public String getLocation() {
		return location;
	}

	/*
	 * @param property location to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setLocation(String location) {
		this.location = location;
	}

	/*
	 * @return current maximum number of guest of string type
	 * 
	 * */
	public String getMaxGuests() {
		return maxGuests;
	}

	/*
	 * @param property maximum number of guest to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setMaxGuests(String maxGuests) {
		this.maxGuests = maxGuests;
	}

	/*
	 * @return current rooms of integer type
	 * 
	 * */
	public int getNoOfRooms() {
		return noOfRooms;
	}

	/*
	 * @param property total rooms to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setNoOfRooms(int noOfRooms) {
		this.noOfRooms = noOfRooms;
	}

	/*
	 * @return current room size of integer type
	 * 
	 * */
	public double getRoomSize() {
		return roomSize;
	}

	/*
	 * @param property room size to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setRoomSize(double roomSize) {
		this.roomSize = roomSize;
	}

	/*
	 * @return current about property in one line comment of string type
	 * 
	 * */
	public String getPropertyLineContent() {
		return propertyLineContent;
	}

	/*
	 * @param about property in one line comment  to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setPropertyLineContent(String propertyLineContent) {
		this.propertyLineContent = propertyLineContent;
	}

	/*
	 * @return current more about property of string type
	 * 
	 * */
	public String getMoreInfo() {
		return moreInfo;
	}

	/*
	 * @param about property to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setMoreInfo(String moreInfo) {
		this.moreInfo = moreInfo;
	}

	/*
	 * @return current number of beds of integer type
	 * 
	 * */
	public int getNoOfBeds() {
		return noOfBeds;
	}

	/*
	 * @param property total number of beds name to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setNoOfBeds(int noOfbeds) {
		this.noOfBeds = noOfBeds;
	}

	/*
	 * @return current price of integer type
	 * 
	 * */
	public int getPrice() {
		return price;
	}

	/*
	 * @param property price to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setPrice(int price) {
		this.price = price;
	}

	/*
	 * @return current rating of float type
	 * 
	 * */
	public float getRatings() {
		return ratings;
	}

	/*
	 * @param property rating to set(as a float type)
	 * @return void
	 * 
	 * */
	public void setRatings(float ratings) {
		this.ratings = ratings;
	}

	/*
	 * @return current property address of string type
	 * 
	 * */
	public String getAddress() {
		return address;
	}

	/*
	 * @param property address to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setAddress(String address) {
		this.address = address;
	}

	/*
	 * @return current image of string type
	 * 
	 * */
	public String getImage1() {
		return image1;
	}


	/*
	 * @param property image URL to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setImage1(String image1) {
		this.image1 = image1;
	}


	/*
	 * @return current image of string type
	 * 
	 * */
	public String getImage2() {
		return image2;
	}

	/*
	 * @param property image URL to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setImage2(String image2) {
		this.image2 = image2;
	}


	/*
	 * @return current image of string type
	 * 
	 * */
	public String getImage3() {
		return image3;
	}

	/*
	 * @param property image URL to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setImage3(String image3) {
		this.image3 = image3;
	}


	/*
	 * @return current image of string type
	 * 
	 * */
	public String getImage4() {
		return image4;
	}

	/*
	 * @param property image URL to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setImage4(String image4) {
		this.image4 = image4;
	}


	/*
	 * @return current image of string type
	 * 
	 * */
	public String getImage5() {
		return image5;
	}

	/*
	 * @param property image URL to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setImage5(String image5) {
		this.image5 = image5;
	}


	/*
	 * {@link java.lang.class#toString()} method.
	 * @return string representation of this action
	 */
	@Override
	public String toString() {
		return "Property [propertyid=" + propertyid + ", propertyName=" + propertyName + ", propertyType="
				+ propertyType + ", location=" + location + ", maxGuests=" + maxGuests + ", address=" + address
				+ ", rooms=" + noOfRooms + ", roomSize=" + roomSize + ", propertyLineContent=" + propertyLineContent
				+ ", moreInfo=" + moreInfo + ", noOfBeds=" + noOfBeds + ", price=" + price + ", ratings=" + ratings
				+ ", image1=" + image1 + ", image2=" + image2 + ", image3=" + image3 + ", image4=" + image4
				+ ", image5=" + image5 + "]";
	}


	

	
	
	
	
	
	
	
}
