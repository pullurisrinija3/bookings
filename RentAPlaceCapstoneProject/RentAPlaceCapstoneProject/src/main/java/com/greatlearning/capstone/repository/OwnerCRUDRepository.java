package com.greatlearning.capstone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.capstone.entity.Owner;

@Repository
public interface OwnerCRUDRepository extends JpaRepository<Owner,String> {

}
