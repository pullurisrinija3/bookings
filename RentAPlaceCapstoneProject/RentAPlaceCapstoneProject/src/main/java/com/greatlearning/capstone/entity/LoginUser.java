package com.greatlearning.capstone.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LoginUser {

	/*
	 * <h1>The table structure for user login</h1>
	 * 
	 * @see #email => email is the login user email  
	 * @see #password => password is the  login user password
	 * 
	 * 
	 *  @annotation => @Entity -- Tells the spring boot to create the table with name "user login" as class name
	 *  @annotation => @GeneratedValue -- specifies how to generate value for a particular column 
	 *          GenerationType.IDENTITY -- It's similar to AUTO generation type. i.e.., Auto_increment is generated for particular column 
	 * */
	@Id
	private String email;
	private String password;

	/*
	 * 
	 * Default constructor of login user class
	 * 
	 *  */
	public LoginUser() {
		// TODO Auto-generated constructor stub
	}

	/*
	 *parameterised constructor of login user class
	 * @param args unused
	 * 
	 * */
	public LoginUser(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	
	/*
	 * @return user email of string type
	 * 
	 * */
	public String getEmail() {
		return email;
	}

	/*
	 * @param user email to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setEmail(String email) {
		this.email = email;
	}

	/*
	 * @return user password of string type
	 * 
	 * */
	public String getPassword() {
		return password;
	}

	/*
	 * @param user password to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setPassword(String password) {
		this.password = password;
	}

	/*
	 * {@link java.lang.class#toString()} method.
	 * @return string representation of this action
	 */
	@Override
	public String toString() {
		return "LoginUser [email=" + email + ", password=" + password + "]";
	}


}
