package com.greatlearning.capstone.entity;

public class ResponsePage {

	/*
	 * <h1>The table structure for response page</h1>
	 * 
	 * @see #message => message is the response message  
	 * @see #description => description is the description of message
	 * 
	 * 
	 *  @annotation => @Entity -- Tells the spring boot to create the table with name "Response page" as class name
	 *  @annotation => @GeneratedValue -- specifies how to generate value for a particular column 
	 *          GenerationType.IDENTITY -- It's similar to AUTO generation type. i.e.., Auto_increment is generated for particular column 
	 * */
	private Messages message;
	private String description;

	/*
	 * 
	 * Default constructor of response page class
	 * 
	 *  */
	public ResponsePage() {
		// TODO Auto-generated constructor stub
	}

	/*
	 *parameterised constructor of response page class
	 * @param args unused
	 * 
	 * */
	public ResponsePage(Messages message, String description) {
		super();
		this.message = message;
		this.description = description;
	}

	public Messages getMessage() {
		return message;
	}

	public void setMessage(Messages message) {
		this.message = message;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*
	 * {@link java.lang.class#toString()} method.
	 * @return string representation of this action
	 */
	@Override
	public String toString() {
		return "ResponsePage [message=" + message + ", description=" + description + "]";
	}


}
