package com.greatlearning.capstone.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.capstone.entity.OwnerLogin;
import com.greatlearning.capstone.repository.OwnerLoginRepository;

@Service
public class OwnerLoginService {
	
	/*
	 * @annotation => @Autowired -- is used to establish connection between classess.
	 * Here we have established connection to
	 * AdminDetailRepository
	 * 
	 */
	@Autowired
	private OwnerLoginRepository adminDetailRepository;
	
	/*
	 * ADMIN REGISTER
	 * Methods used to list admin details into database
	 * @return adminList
	 *  
	 */
	public List<OwnerLogin> getAllOwner() {
		List<OwnerLogin> adminList=new ArrayList<OwnerLogin>();
		this.adminDetailRepository.findAll().forEach(admin->adminList.add(admin));
		return adminList;
	}

}
