package com.greatlearning.capstone.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.capstone.entity.UserRegistration;
import com.greatlearning.capstone.repository.UserRegistrationRepository;

@Service
public class UserRegistrationService {
	
	/*
	 * @annotation => @Autowired -- is used to establish connection between classess.
	 * Here we have established connection to
	 * UserDetailRepository
	 * 
	 */
	@Autowired
	private UserRegistrationRepository userDetailRepository;
	
	/*
	 * USER REGISTER
	 * Methods used to add a new user to database
	 * @return boolean
	 *  
	 */
	public boolean insertUser(UserRegistration userDetail) {
		if(!this.userDetailRepository.existsById(userDetail.getUserId())) {
			this.userDetailRepository.save(userDetail);
			return true;
		}
		return false;
	}

	/*
	 * find by email from user table database
	 * @param email of string type
	 * @return List<LoginUser>
	 * 
	 */
	public UserRegistration getFindByEmail(String email) {
		return userDetailRepository.findByEmail(email);
	}
}
