package com.greatlearning.capstone.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ownerlogin")
public class OwnerLogin {

	/*
	 * <h1>The table structure for user login</h1>
	 * 
	 * @see #adminId => adminId for admin
	 * @see #name => name is the name of the admin user
	 * @see #email => email is the login admin email
	 * @see #phone => phone is the phone number of the admin user  
	 * @see #password => password is the  login admin password
	 * 
	 * 
	 *  @annotation => @Entity -- Tells the spring boot to create the table with name "user login" as class name
	 *  @annotation => @Id -- creates "adminId" as a primary key in loginuser table
	 *  @annotation => @GeneratedValue -- specifies how to generate value for a particular column 
	 *          GenerationType.IDENTITY -- It's similar to AUTO generation type. i.e.., Auto_increment is generated for particular column 
	 * */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ownerid")
	private int ownerId;
	private String name;
	private String email;
	private String phone;
	private String password;

	/*
	 * 
	 * Default constructor of admin class
	 * 
	 *  */
	public OwnerLogin() {
		// TODO Auto-generated constructor stub
	}

	/*
	 *parameterised constructor of the admin class
	 * @param args unused
	 * 
	 * */
	public OwnerLogin(String name, String email, String phone, String password) {
		super();
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.password = password;
	}

	/*
	 * @return admin_id of integer type
	 * 
	 * */
	public int getOwnerId() {
		return ownerId;
	}

	/*
	 * @param admin id to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}

	/*
	 * @return admin name of string type
	 * 
	 * */
	public String getName() {
		return name;
	}

	/*
	 * @param admin name to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setName(String name) {
		this.name = name;
	}

	/*
	 * @return admin email of string type
	 * 
	 * */
	public String getEmail() {
		return email;
	}

	/*
	 * @param admin email to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setEmail(String email) {
		this.email = email;
	}

	/*
	 * @return admin user phone number of string type
	 * 
	 * */
	public String getPhone() {
		return phone;
	}

	/*
	 * @param admin phone number to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/*
	 * @return admin password of string type
	 * 
	 * */
	public String getPassword() {
		return password;
	}

	/*
	 * @param admin password to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setPassword(String password) {
		this.password = password;
	}

	/*
	 * {@link java.lang.class#toString()} method.
	 * @return string representation of this action
	 */
	@Override
	public String toString() {
		return "OwnerLogin [ownerId=" + ownerId + ", name=" + name + ", email=" + email + ", phone=" + phone
				+ ", password=" + password + "]";
	}


}
