package com.greatlearning.capstone.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.capstone.entity.BookedProperty;

@Repository
public interface PropertyReservationRepository extends CrudRepository<BookedProperty, Integer>{
	/*
	 * GET ALL THE DATA
	 * Methods for search operation => fetch all the data
	 * @return List<BookedProperty>
	 * 
	 */
	List<BookedProperty> findAll();
}
