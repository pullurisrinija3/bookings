package com.greatlearning.capstone.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.capstone.entity.LoginUser;
import com.greatlearning.capstone.entity.Messages;
import com.greatlearning.capstone.entity.Property;
import com.greatlearning.capstone.entity.ResponsePage;
import com.greatlearning.capstone.entity.UserRegistration;
import com.greatlearning.capstone.repository.PropertyRepository;
import com.greatlearning.capstone.service.LoginUserService;
import com.greatlearning.capstone.service.UserRegistrationService;
import com.greatlearning.capstone.entity.User;
import com.greatlearning.capstone.service.UserCRUDService;

@RestController
@RequestMapping("/users")
@CrossOrigin
public class UserController {
	
	/*
	 * @annotation => @Autowired -- Is used to establish connection between classess .
	 * Here, we have connected to 
	 * AuthenticationManager
	 * 
	 */
	@Autowired
	private AuthenticationManager authenticationManager;

	/*
	 * @annotation => @Autowired -- Is used to establish connection between classess .
	 * Here, we have connected to 
	 * UserDetailService
	 * 
	 */
	@Autowired
	private UserRegistrationService userDetailService;

	/*
	 * @annotation => @Autowired -- Is used to establish connection between classess .
	 * Here, we have connected to 
	 * LoginUserService
	 * 
	 */
	@Autowired
	private LoginUserService loginUserService;

	/*
	 * @annotation => @Autowired -- Is used to establish connection between classess .
	 * Here, we have connected to 
	 * PasswordEncoder
	 * 
	 */
	@Autowired
	private PasswordEncoder encoder;

	
	/*
	 *  User REGISTER
	 *  value="/register" is the url for connecting angular in backend 
	 *  This url is used to insert the registered value from frontend to store into database. 
	 *  then the password is encoded for security purpose and then stored in database
	 *  
	 */
	@PostMapping("/register")
	public ResponsePage registerUser(@RequestBody UserRegistration userDetail) throws Exception{
		System.out.println("Registering the user......");
		System.out.println(userDetail);
		try {
			if(this.userDetailService.insertUser(userDetail)) {
				LoginUser user = new LoginUser();
				user.setEmail(userDetail.getEmail());
				user.setPassword(encoder.encode(userDetail.getPassword()));
				this.loginUserService.insertUser(user);		
				return new ResponsePage(Messages.SUCCESS, "Registration successfull");
			}else
				return new ResponsePage(Messages.FAILURE, "Registration unsuccessfull");
		}catch(Exception e) {
			System.out.println("Registration unsuccessfull");
			return new ResponsePage(Messages.FAILURE, "Registration unsuccessfull");
		}
	}

	/*
	 *  User Login Validation
	 *  value="/authenticate" is the url for connecting angular in backend 
	 *  This url is used to validate the user details to login.
	 *  @return success message
	 *  
	 *  
	 */
	@PostMapping("/authenticate")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody LoginUser user) throws Exception
	{
		System.out.println("authenticate ......");
		System.out.println(user);
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));
		}
		catch(BadCredentialsException e)
		{
			System.out.println("login failed");
			return ResponseEntity.badRequest().body(new ResponsePage(Messages.FAILURE,"Login failed"));
		}
		return ResponseEntity.ok().body(new ResponsePage(Messages.SUCCESS,"Login success"));
	}
	
//CRUD ON USERS
	@Autowired
	private UserCRUDService userservice;
	
	@GetMapping(value = "getAllUserDetails",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> getAllUsers()
	{
	return userservice.getAllUserDetails();
	}
	@PostMapping(value = "storeUserDetails",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeUserInfo(@RequestBody User user) 
	{	
	return userservice.storeUserDetails(user);
	}
	
	@DeleteMapping(value = "deleteUserDetails/{Email}")
	public String deleteUserInfo(@PathVariable("Email") String Email)
	{
	return userservice.deleteUserDetails(Email);
	}
	
	@PatchMapping(value = "updateUserDetails")
	public String updateUserInfo(@RequestBody User user) 
	{
	return userservice.updateUserDetails(user);
	}	
}
