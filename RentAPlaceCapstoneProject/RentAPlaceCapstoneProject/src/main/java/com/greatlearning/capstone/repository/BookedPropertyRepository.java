package com.greatlearning.capstone.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.capstone.entity.BookedProperty;

@Repository
public interface BookedPropertyRepository extends CrudRepository<BookedProperty, Integer>{

	/*
	 * Methods to find the booked data using booked_id
	 * @param booked_id id of integer type
	 * @return boolean
	 * 
	 */
	BookedProperty findById(int booked_id);
}
