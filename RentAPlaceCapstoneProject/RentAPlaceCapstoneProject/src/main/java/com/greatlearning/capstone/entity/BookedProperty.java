package com.greatlearning.capstone.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="bookedProperty")
public class BookedProperty {
	
	/*
	 * <h1>The table structure for storing Booked Property Data</h1>
	 * 
	 * @see #booked_id => booked_id for booked property
	 * @see #user_name => user_name is the registered user name 
	 * @see #user_email => user_email is the registered user email
	 * @see #user_phone => user_phone is the registered user phone number  
	 * @see #room_type => room_type is the  property type like Appartment,Villa,Resort
	 * @see #room_name  => room_name is the property name
	 * @see #date_from => date_from is the booked date for reservation
	 * @see #due_date => due_date is the booked leaving date for reservation
	 * @see #no_of_nights => no_of_nights is the total number of nights booked for reservation
	 * @see #no_of_rooms => no_of_rooms is the total number of rooms booked for reservation
	 * @see #subtotal => subtotal is the total amount without adding GST
	 * @see #tax => tax is the amount for tax
	 * @see #total => total is the total amount with added GST
	 * @see #property_address => property_address is the address of the property
	 * @see #property_phone => property_phone is the phone number of property owner
	 * @see #property_email => property_address is the owner's email
	 * @see #status => status is the status of booked property confirmation (Approved or Pending)
	 * 
	 *  @annotation => @Entity -- Tells the spring boot to create the table with name "bookedproperty" as class name
	 *  @annotation => @Id -- creates "booked_id" as a primary key in bookedproperty table
	 *  @annotation => @GeneratedValue -- specifies how to generate value for a particular column 
	 *          GenerationType.IDENTITY -- It's similar to AUTO generation type. i.e.., Auto_increment is generated for particular column 
	 * */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int bookedid;
	private String username;
	private String userEmail;
	private String userPhone;
	private String roomType;
	private String roomName;
	private String datefrom;
	private String duedate;
	private int noOfNights;
	private int noOfRooms;
	private int subtotal;
	private int tax;
	private int total;
	private String propertyAddress;
	private String propertyPhone;
	private String propertyEmail;
	private String status;
	
	/*
	 * 
	 * Default constructor of booked property class
	 * 
	 *  */
	public BookedProperty() {
		
	}

	/*
	 *parameterised constructor of the booked property class
	 * @param args unused
	 * 
	 * */
	public BookedProperty(String username, String userEmail, String userPhone, String roomType, String roomName,
			String datefrom, String duedate, int noOfNights, int noOfRooms, int subtotal, int tax, int total,
			String propertyAddress, String propertyPhone, String propertyEmail,String status) {
		super();
		this.username = username;
		this.userEmail = userEmail;
		this.userPhone = userPhone;
		this.roomType = roomType;
		this.roomName = roomName;
		this.datefrom = datefrom;
		this.duedate = duedate;
		this.noOfNights = noOfNights;
		this.noOfRooms = noOfRooms;
		this.subtotal = subtotal;
		this.tax = tax;
		this.total = total;
		this.propertyAddress = propertyAddress;
		this.propertyPhone = propertyPhone;
		this.propertyEmail = propertyEmail;
		this.status=status;
	}

	/*
	 * @return current booked_id of integer type
	 * 
	 * */
	public int getBookedid() {
		return bookedid;
	}

	/*
	 * @param booked id to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setBookedid(int bookedid) {
		this.bookedid = bookedid;
	}

	/*
	 * @return current user name of string type
	 * 
	 * */
	public String getUsername() {
		return username;
	}

	/*
	 * @param booked user name to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setUsername(String username) {
		this.username = username;
	}

	/*
	 * @return current user email of string type
	 * 
	 * */
	public String getUserEmail() {
		return userEmail;
	}

	/*
	 * @param booked user email to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	/*
	 * @return current user phone number of string type
	 * 
	 * */
	public String getUserPhone() {
		return userPhone;
	}

	/*
	 * @param booked user phone number to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	/*
	 * @return current booked room type of string type
	 * 
	 * */
	public String getRoomType() {
		return roomType;
	}

	/*
	 * @param property room type to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	/*
	 * @return current booked room name of string type
	 * 
	 * */
	public String getRoomName() {
		return roomName;
	}

	/*
	 * @param property room name to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	/*
	 * @return current starting date for reservation of string type
	 * 
	 * */
	public String getDatefrom() {
		return datefrom;
	}

	/*
	 * @param property starting date to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setDatefrom(String datefrom) {
		this.datefrom = datefrom;
	}

	/*
	 * @return current leaving date for reservation of string type
	 * 
	 * */
	public String getDuedate() {
		return duedate;
	}

	/*
	 * @param property leaving date to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}

	/*
	 * @return total number of nights stay booked of integer type
	 * 
	 * */
	public int getNoOfNights() {
		return noOfNights;
	}

	/*
	 * @param property total number of nights stay booked to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setNoOfNights(int noOfNights) {
		this.noOfNights = noOfNights;
	}

	/*
	 * @return current total number of rooms booked of integer type
	 * 
	 * */
	public int getNoOfRooms() {
		return noOfRooms;
	}

	/*
	 * @param property total number of rooms booked to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setNoOfRooms(int noOfRooms) {
		this.noOfRooms = noOfRooms;
	}

	/*
	 * @return current sub total without adding GST of integer type
	 * 
	 * */
	public int getSubtotal() {
		return subtotal;
	}

	/*
	 * @param property total without adding GST to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setSubtotal(int subtotal) {
		this.subtotal = subtotal;
	}

	/*
	 * @return current tax calculated from the subtotal of integer type
	 * 
	 * */
	public int getTax() {
		return tax;
	}

	/*
	 * @param property tax to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setTax(int tax) {
		this.tax = tax;
	}

	/*
	 * @return current total amount added Tax of integer type
	 * 
	 * */
	public int getTotal() {
		return total;
	}

	/*
	 * @param property total  amount added Tax to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setTotal(int total) {
		this.total = total;
	}

	/*
	 * @return current property address of string type
	 * 
	 * */
	public String getPropertyAddress() {
		return propertyAddress;
	}

	/*
	 * @param property address to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setPropertyAddress(String propertyAddress) {
		this.propertyAddress = propertyAddress;
	}

	/*
	 * @return current owner phone of string type
	 * 
	 * */
	public String getPropertyPhone() {
		return propertyPhone;
	}

	/*
	 * @param owner phone to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setPropertyPhone(String propertyPhone) {
		this.propertyPhone = propertyPhone;
	}

	/*
	 * @return current property email of string type
	 * 
	 * */
	public String getPropertyEmail() {
		return propertyEmail;
	}

	/*
	 * @param owner email to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setPropertyEmail(String propertyEmail) {
		this.propertyEmail = propertyEmail;
	}
	
	/*
	 * @return current property status of string type
	 * 
	 * */
	public String getStatus() {
		return status;
	}

	/*
	 * @param property status to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setStatus(String status) {
		this.status = status;
	}

	/*
	 * {@link java.lang.class#toString()} method.
	 * @return string representation of this action
	 */
	@Override
	public String toString() {
		return "BookedProperty [booked_id=" + bookedid + ", username=" + username + ", userEmail=" + userEmail
				+ ", userPhone=" + userPhone + ", roomType=" + roomType + ", roomName=" + roomName
				+ ", datefrom=" + datefrom + ", duedate=" + duedate + ", noOfNights=" + noOfNights
				+ ", noOfRooms=" + noOfRooms + ", subtotal=" + subtotal + ", tax=" + tax + ", total=" + total
				+ ", propertyAddress=" + propertyAddress + ", propertyPhone=" + propertyPhone + ", propertyEmail="
				+ propertyEmail + ", status=" + status + "]";
	}

	
	
	
}

