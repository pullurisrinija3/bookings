package com.greatlearning.capstone.entity;



public class PropertyReservation {
	
	/*
	 * <h1>The table structure for storing Booked Property Data</h1>
	 * 
	 * 
	 * @see #date_from => date_from is the booked date for reservation
	 * @see #due_date => due_date is the booked leaving date for reservation
	 * @see #no_of_nights => no_of_nights is the total number of nights booked for reservation
	 * @see #no_of_rooms => no_of_rooms is the total number of rooms booked for reservation
	 * @see #subtotal => subtotal is the total amount without adding GST
	 * @see #tax => tax is the amount for tax
	 * @see #total => total is the total amount with added GST
	 *
	 *  @annotation => @Entity -- Tells the spring boot to create the table with name "bookedpropertydata" as class name
	 *  @annotation => @GeneratedValue -- specifies how to generate value for a particular column 
	 *          GenerationType.IDENTITY -- It's similar to AUTO generation type. i.e.., Auto_increment is generated for particular column 
	 * */
	private String datefrom;
	private String duedate;
	private int noOfNights;
	private int noOfRooms;
	private int subtotal;
	private int tax;
	private int total;
	
	/*
	 * 
	 * Default constructor of booked property data class
	 * 
	 *  */
	public PropertyReservation() {
		
	}

	/*
	 *parameterised constructor of the booked property data class
	 * @param args unused
	 * 
	 * */
	public PropertyReservation(String datefrom, String duedate, int noOfNights, int noOfRooms, int subtotal, int tax,
			int total) {
		super();
		this.datefrom = datefrom;
		this.duedate = duedate;
		this.noOfNights = noOfNights;
		this.noOfRooms = noOfRooms;
		this.subtotal = subtotal;
		this.tax = tax;
		this.total = total;
	}

	/*
	 * @return current starting date for reservation of string type
	 * 
	 * */
	public String getDatefrom() {
		return datefrom;
	}

	/*
	 * @param property starting date to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setDatefrom(String datefrom) {
		this.datefrom = datefrom;
	}

	/*
	 * @return current leaving date for reservation of string type
	 * 
	 * */
	public String getDuedate() {
		return duedate;
	}

	/*
	 * @param property leaving date to set(as a string type)
	 * @return void
	 * 
	 * */
	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}

	/*
	 * @return total number of nights stay booked of integer type
	 * 
	 * */
	public int getNoOfNights() {
		return noOfNights;
	}

	/*
	 * @param property total number of nights stay booked to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setNoOfNights(int noOfNights) {
		this.noOfNights = noOfNights;
	}

	/*
	 * @return current total number of rooms booked of integer type
	 * 
	 * */
	public int getNoOfRooms() {
		return noOfRooms;
	}

	/*
	 * @param property total number of rooms booked to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setNoOfRooms(int noOfRooms) {
		this.noOfRooms = noOfRooms;
	}

	/*
	 * @return current sub total without adding GST of integer type
	 * 
	 * */
	public int getSubtotal() {
		return subtotal;
	}

	/*
	 * @param property total without adding GST to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setSubtotal(int subtotal) {
		this.subtotal = subtotal;
	}

	/*
	 * @return current tax calculated from the subtotal of integer type
	 * 
	 * */
	public int getTax() {
		return tax;
	}

	/*
	 * @param property tax to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setTax(int tax) {
		this.tax = tax;
	}

	/*
	 * @return current total amount added Tax of integer type
	 * 
	 * */
	public int getTotal() {
		return total;
	}

	/*
	 * @param property total  amount added Tax to set(as a integer type)
	 * @return void
	 * 
	 * */
	public void setTotal(int total) {
		this.total = total;
	}

	/*
	 * {@link java.lang.class#toString()} method.
	 * @return string representation of this action
	 */
	@Override
	public String toString() {
		return "BookPropertyData [datefrom=" + datefrom + ", duedate=" + duedate + ", noOfNights=" + noOfNights
				+ ", noOfRooms=" + noOfRooms + ", subtotal=" + subtotal + ", tax=" + tax + ", total=" + total + "]";
	}
	
	
	
}
