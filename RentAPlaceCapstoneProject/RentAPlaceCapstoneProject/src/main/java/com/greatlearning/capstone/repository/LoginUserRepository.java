package com.greatlearning.capstone.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.capstone.entity.LoginUser;

@Repository
public interface LoginUserRepository extends CrudRepository<LoginUser, String> {

	/*
	 * Method with parameters of string types used to find loginuser
	 * @param firstname(string)
	 * @param lastname(string)
	 * 0@param email (String)
	 * @param phone(string)
	 * @param password(string)
	 * @param confirm_password(string)
	 * @return user 
	 * 
	 * */
}
