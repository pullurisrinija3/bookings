package com.greatlearning.capstone.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.greatlearning.capstone.entity.LoginUser;
import com.greatlearning.capstone.repository.LoginUserRepository;

@Service
public class UserService implements UserDetailsService{

	/*
	 * @annotation => @Autowired -- is used to establish connection between classess.
	 * Here we have established connection to
	 * LoginUserRepository
	 * 
	 */
	@Autowired
	private LoginUserRepository repo;
	
	/*
	 * this method is to check the email and password for user login
	 * @param username for string type
	 * return user
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Optional<LoginUser> optuser = this.repo.findById(username);

		return optuser.map(acc ->{

			User user = new User(acc.getEmail(), 
					acc.getPassword(),
					new ArrayList<>());
			return user;
		})
		.orElseThrow(()-> new UsernameNotFoundException("User does not exist with username "+username))
				;

	}

}
