package com.greatlearning.capstone.controller;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.capstone.RentAPlaceCapstoneProjectApplication;
import com.greatlearning.capstone.entity.BookedProperty;
import com.greatlearning.capstone.entity.Property;
import com.greatlearning.capstone.entity.PropertyReservation;
import com.greatlearning.capstone.entity.UserRegistration;
import com.greatlearning.capstone.repository.BookedPropertyRepository;
import com.greatlearning.capstone.repository.PropertyRepository;
import com.greatlearning.capstone.repository.PropertyReservationRepository;
import com.greatlearning.capstone.service.EmailSenderService;
import com.greatlearning.capstone.service.PropertyService;
import com.greatlearning.capstone.service.UserRegistrationService;

@RestController
@CrossOrigin
public class PropertyController {

	/*
	 * @annotation => @Autowired -- Is used to establish connection between classess .
	 * Here, we have connected to 
	 * PropertyRepository
	 * PropertyService
	 * UserDetailService
	 * BookedDataProperty
	 * EmailSenderService
	 * 
	 */
	@Autowired
	private PropertyRepository propertyRepository;
	
	@Autowired
	private PropertyService propertyService;
	
	@Autowired
	private UserRegistrationService userDetailService;
	
	@Autowired
	private PropertyReservationRepository bookedDataProperty;
	
	@Autowired
	private BookedPropertyRepository bookedPropertyRepository;
	
	@Autowired
	private EmailSenderService service;
	
	/*
	 * TO DISPLAY ALL PROPERTY
	 * This method is used to display all the property to the user and admin webpage
	 * @return List<Property>
	 *
	 * */
	@GetMapping("/getnoOfRooms")
	public List<Property> getAllProperty() {
		return (List<Property>) propertyRepository.findAll();
	}
	
	/*
	 * TO DISPLAY BOOKED PROPERTY
	 * This method is used to display all booked property to the admin webpage
	 * @return List<BookedProperty>
	 *
	 * */
	@GetMapping("/getBookedData")
	public List<BookedProperty> getAllBookedData() {
		return (List<BookedProperty>) bookedDataProperty.findAll();
	}
	
	/*
	 * TO DISPLAY SELECTED PROPERTY BY ID
	 * This method is used to display selected property to the user and admin webpage for more information
	 * @return List<Property>
	 *
	 * */
	@GetMapping("/propertyid/{id}")
	public Property getPropertyById(@PathVariable int id){
		return propertyService.getFindById(id);
	}
	
	/*
	 * TO DISPLAY SPECIFIC PROPERTY LOCATION A SELECTED LOCATION
	 * This method is used to display selected property to the user for property reservation
	 * @return List<Property>
	 *
	 * */
	@GetMapping("/prerpertyLocation/{location}")
	public List<Property> getPropertyByLocation(@PathVariable String location){	
		return propertyService.getFindByLocation(location);
	}
	
	/*
	 * TO DISPLAY SPECIFIC PROPERTY DETAILS USING PROPERTY TYPE
	 * This method is used to display selected property to the user for property reservation
	 * @return List<Property>
	 *
	 * */
	@GetMapping("/prerpertyType/{type}")
	public List<Property> getPropertyByType(@PathVariable String type){	
		return propertyService.getFindByType(type);
	}
	
	/*
	 * BOOKED PROPERTY RESERVATION DETIALS STORED IN DATABASE
	 * This method is used to store a booked details to the database
	 * @return List<BookedProperty>
	 *
	 * */
	@PostMapping("/bookProperty/{email}/{id}")
	public BookedProperty getStoreBookedProperty(@PathVariable int id,@PathVariable String email,@RequestBody PropertyReservation bookPropertyData) {
		System.out.println(id+"---"+email);
		Property property=propertyService.getFindById(id);
		UserRegistration userDetail=userDetailService.getFindByEmail(email);
		System.out.println(property);
		System.out.println(userDetail);
		BookedProperty bookedProperty=new BookedProperty();
		bookedProperty.setUsername(userDetail.getFirstname() +" "+userDetail.getLastname());
		bookedProperty.setUserEmail(userDetail.getEmail());
		bookedProperty.setUserPhone(userDetail.getPhone());
		bookedProperty.setRoomType(property.getPropertyType());
		bookedProperty.setRoomName(property.getPropertyName());
		bookedProperty.setDatefrom(bookPropertyData.getDatefrom());
		bookedProperty.setDuedate(bookPropertyData.getDuedate());
		bookedProperty.setNoOfNights(bookPropertyData.getNoOfNights());
		bookedProperty.setNoOfRooms(bookPropertyData.getNoOfRooms());
		bookedProperty.setSubtotal(bookPropertyData.getSubtotal());
		bookedProperty.setTax(bookPropertyData.getTax());
		bookedProperty.setTotal(bookPropertyData.getTotal());
		bookedProperty.setPropertyAddress(property.getLocation());
		bookedProperty.setPropertyPhone("9345754701");
		bookedProperty.setPropertyEmail("prasanthhero40@gmail.com");
		bookedProperty.setStatus("Pending");
		propertyService.storeBookedProperty(bookedProperty);
		System.out.println(bookedProperty);
		System.out.println(bookedProperty.getBookedid());
		return bookedProperty;
	}
	
	/*
	 * TO ADD A PROPERTY
	 * This method is used to add a new property to the database
	 * 
	 * */
	@PostMapping("/addproperty")
	public Property getToAddProperty(@RequestBody Property property) {
		System.out.println("New Property Added");
		return propertyService.getToAddPropertyData(property);
	}
	
	/*
	 * DELETE A PROPERTY BY ID
	 * this method is used to delete a selected property from owner webpage
	 * @return deletedConfirmation
	 * */
	@DeleteMapping("/deleteProperty/{id}")
	public boolean getDetetePropertyById(@PathVariable int id) {
		System.out.println(id);
		if (propertyRepository.findById(id) != null) {
			Property property= propertyRepository.findById(id);
			propertyRepository.delete(property);
			System.out.println("Property deleted");
			return true;
		}
		else {
			return false;
		}
		
	}
	
	/*
	 * EDIT A PROPERTY BY ID
	 * this method is used to edit a selected property from owner webpage then stored in database
	 * @return edited Property
	 * */
	@PostMapping("/updateProperty")
	public boolean getUpdatedProperty(@RequestBody Property property) {
		if(propertyService.updateProperty(property)) {
			System.out.println("Property Updated");
			System.out.println(property);
			return true;
		}
		else {
			return false;
		}
	}
	
	/*
	 * TO DISPLAY BOOKED PROPERTY PENDING STATUS 
	 * This method is used to display the booked property pending status details 
	 * @return BookedProperty pending status
	 */
	@GetMapping("/prorpertyStatusPendingAll")
	public List<BookedProperty> getprorpertyStatusPendingAll(){
		return propertyService.getpropertyStatusPendingAll();
	}
	
	/*
	 * BOOKED PROPERTY APPROVAL STATUS FROM OWNER SIDE
	 * This method is used to approve the booked property and sends the confirmation sent to mail
	 * @param bookde_id of type BookedProperty
	 * @return confirmation mail  
	 */
	@PostMapping("/prorpertyStatusApproval")
	public boolean getProrpertyStatusApproval(@RequestBody int id) {
		BookedProperty bookeddata= bookedPropertyRepository.findById(id);
		service.emailForOwnerToUser("prasanth741999@gmail.com", "Hello Sir,\n Your Property Booking Confirmed","Booking Confirmation Approved");
		return propertyService.getPropertyStatusApproval(id);
	}
	
	
	

	/*
	 * CONFIMATION MAIL AND BOOKED SUMMARY
	 * In this method we are getting the values from frontend and adding the values into the database
	 * sendSimpleEmail('','','')  is the method used to send email to admin whenever user booked a property once confirmed
	 * @param toEmail (string) -> to email details
	 * @param body (string)= -> body of the  mail
	 * @param subject (string) -> subject of the mail
	 * @return BookedProperty Details
	 * 
	 */
	@EventListener(RentAPlaceCapstoneProjectApplication.class)
	@PostMapping("/bookedPropertyId")
	public BookedProperty getPropertyByBookedId(@RequestBody int id) throws MessagingException {
		BookedProperty booked_data= bookedPropertyRepository.findById(id);
		service.emailForUsertoOwner(booked_data.getUserEmail(),"havenlife3181@gmail.com",
				"New booking Confirmataion\nProperty Name :"+booked_data.getRoomName()+"\n\nName :"+booked_data.getUsername()+"\nPhone :"+booked_data.getUserPhone()
				+"\n\n     PRICE     \n--------------------------\nSubtotal :"+booked_data.getSubtotal()+"\nTax     :"+booked_data.getTax()+
				"\n\n--------------------------\n\n TOTAL   :"+booked_data.getTotal()+"\n\n************\n*****************\n**************************",
				"Property Booking Confirmation From "+booked_data.getDatefrom()+" - "+booked_data.getDuedate());
		return propertyService.getFindByBookedId(id);
	}
	
	
}
