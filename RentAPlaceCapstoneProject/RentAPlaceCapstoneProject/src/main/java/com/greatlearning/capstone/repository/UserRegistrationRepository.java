package com.greatlearning.capstone.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.capstone.entity.UserRegistration;

@Repository
public interface UserRegistrationRepository extends CrudRepository<UserRegistration, Integer> {

	/*
	 * Method with single parameter of string type to find user with their  email.
	 * @param email(string)
	 * @return User
	 */
	UserRegistration findByEmail(String email);
}
