package com.greatlearning.capstone.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderService {

	/*
	 * @annotation => @Autowired -- is used to establish connection between classess.
	 * Here we have established connection to
	 * JavaMailSender
	 * 
	 */
	@Autowired
	private JavaMailSender mailSender;

	/*
	 * MAIL MODULE FOR OWNER TO USER
	 * Method sendSimpleEmail('','','') with three parameter is used to send mail to admin every user confirmation
	 * @param toEmail(string) is the mail id of the admin to which mail has to be send.
	 * @param body(string) contains the body of the mail
	 * @param subject(string) contains the subject of the mail.
	 * @return void
	 * 
	 */
	public void emailForOwnerToUser(String toEmail,
			String body,
			String subject) {
		SimpleMailMessage message = new SimpleMailMessage();

		message.setFrom("havenlife@gmail.com");
		message.setTo(toEmail);
		message.setText(body);
		message.setSubject(subject);

		/*
		 * setting the values to the message that has to be sent to admin.
		 * 
		 */
		mailSender.send(message);
		System.out.println("Mail Send...");
	}

	/*
	 * MAIL MODULE FOR USER OT OWNER
	 * Method emailForUsertoOwner('','','','') with four parameter is used to send mail to admin every user booked detail to admin email
	 * @param fromEmail(string) is the mail id of the user to which mail has to be send.
	 * @param toEmail(string) is the mail id of the admin to which mail has to be send.
	 * @param body(string) contains the body of the mail
	 * @param subject(string) contains the subject of the mail.
	 * @return void
	 * 
	 */
	public void emailForUsertoOwner(String fromEmail, String toEmail,
			String body,
			String subject) {
		SimpleMailMessage message = new SimpleMailMessage();

		message.setFrom(fromEmail);
		System.out.println(fromEmail);
		message.setTo(toEmail);
		System.out.println(toEmail);
		message.setText(body);
		System.out.println(subject);
		System.out.println(body);
		message.setSubject(subject);

		mailSender.send(message);
		System.out.println("Mail Send...");
	}
}
