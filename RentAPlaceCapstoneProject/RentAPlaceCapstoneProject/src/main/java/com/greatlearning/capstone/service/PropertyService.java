package com.greatlearning.capstone.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.capstone.entity.BookedProperty;
import com.greatlearning.capstone.entity.Property;
import com.greatlearning.capstone.entity.UserRegistration;
import com.greatlearning.capstone.repository.BookedPropertyRepository;
import com.greatlearning.capstone.repository.PropertyRepository;
import com.greatlearning.capstone.repository.PropertyReservationRepository;

@Service
public class PropertyService {
	/*
	 * @annotation => @Autowired -- is used to establish connection between classess.
	 * Here we have established connection to
	 * PropertyRepository
	 * 
	 */
	@Autowired
	private PropertyRepository propertyRepository;
	
	/*
	 * @annotation => @Autowired -- is used to establish connection between classess.
	 * Here we have established connection to
	 * BookedPropertyrepository
	 * 
	 */
	@Autowired
	private BookedPropertyRepository bookedPropertyRepository;
	
	/*
	 * @annotation => @Autowired -- is used to establish connection between classess.
	 * Here we have established connection to
	 * BookedDataProperty
	 * 
	 */
	@Autowired
	private PropertyReservationRepository bookedDataProperty;
	
	/*
	 * To display property find by id
	 * @param id of integer type
	 * @return Property
	 * 
	 */
	public Property getFindById(int id) {
		return propertyRepository.findById(id);
	}
	
	/*
	 * To display list of property find by location
	 * @param location of integer type
	 * @return List<Property>
	 * 
	 */
	public List<Property> getFindByLocation(String location) {
		return propertyRepository.findByLocation(location);
	}
	
	/*
	 * To display list of property find by type
	 * @param type of integer type
	 * @return List<Property>
	 * 
	 */
	public List<Property> getFindByType(String type) {
		List<Property> list=propertyRepository.findAll();
		List<Property> searched= new ArrayList<>();
		for(Property lists: list) {
			if(lists.getPropertyType().equals(type)) {
				searched.add(lists);
			}
		}
		return searched;
	}
	
	/*
	 * insert a booked property to database
	 * @param bookedProperty of BookedProperty
	 * @return boolean
	 * 
	 */
	public boolean storeBookedProperty(BookedProperty bookedProperty) {
		if(this.bookedPropertyRepository.save(bookedProperty) != null) {
			return true;
		}
		return false;
	}
	
	/*
	 * To display booked property find by id
	 * @param id of integer type
	 * @return bookedProperty
	 * 
	 */
	public BookedProperty getFindByBookedId(int id) {
		return bookedPropertyRepository.findById(id);
	}
	
	/*
	 * insert a property to database
	 * @param property of Property
	 * @return property
	 * 
	 */
	public Property getToAddPropertyData(Property property) {
		return propertyRepository.save(property);
	}
	
	/*
	 * update a edited property data to database
	 * @param Property of Property
	 * @return boolean
	 * 
	 */
	public boolean updateProperty(Property property) {
		if (this.propertyRepository.existsById(property.getPropertyid())) {
			this.propertyRepository.save(property);
			return true;
		}
		return false;
	}
	
	/*
	 * get list of booked property find by status for pending confirmation
	 * @return List<BookedProperty>
	 * 
	 */
	public List<BookedProperty> getpropertyStatusPendingAll() {
		List<BookedProperty> list=bookedDataProperty.findAll();
		System.out.println(list);
		List<BookedProperty> searched= new ArrayList<>();
		for(BookedProperty lists: list) {
			if(lists.getStatus().equals("Pending")) {
				searched.add(lists);
			}
		}
		System.out.println(searched);
		return searched;
	}
	
	/*
	 * get the booked property deatils and owner will approve the booked property 
	 * @param id of integer type
	 * @return List<BookedProperty>
	 * 
	 */
	public boolean getPropertyStatusApproval(int id) {
		BookedProperty booked_property=bookedPropertyRepository.findById(id);
		booked_property.setStatus("Approved");
		bookedPropertyRepository.save(booked_property);
		System.out.println(booked_property);
		return true;
	}
}
