package com.greatlearning.capstone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.capstone.entity.User;

@Repository
public interface UserCRUDRepository  extends JpaRepository<User, String>{

}
