package com.greatlearning.capstone.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.capstone.entity.OwnerLogin;

@Repository
public interface OwnerLoginRepository extends CrudRepository<OwnerLogin, Integer> {

}
