package com.greatlearning.capstone.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.capstone.*;
import com.greatlearning.capstone.entity.Owner;
import com.greatlearning.capstone.entity.OwnerLogin;
import com.greatlearning.capstone.service.OwnerCRUDService;
import com.greatlearning.capstone.entity.LoginUser;
import com.greatlearning.capstone.entity.Messages;
import com.greatlearning.capstone.entity.ResponsePage;
import com.greatlearning.capstone.service.OwnerLoginService;
import com.greatlearning.capstone.service.LoginUserService;

@RestController
@CrossOrigin
@RequestMapping(value="/owner")
public class OwnerController {
	/*
	 * @annotation => @Autowired -- Is used to establish connection between classess .
	 * Here, we have connected to 
	 * AuthenticationManager
	 * 
	 */
	
	@Autowired
	private AuthenticationManager authenticationManager;

	/*
	 * @annotation => @Autowired -- Is used to establish connection between classess .
	 * Here, we have connected to 
	 * AdminDetailService
	 * 
	 */
	@Autowired
	private OwnerLoginService adminDetailService;

	/*
	 * @annotation => @Autowired -- Is used to establish connection between classess .
	 * Here, we have connected to 
	 * LoginUserService
	 * 
	 */
	@Autowired
	private LoginUserService loginUserService;

	/*
	 * @annotation => @Autowired -- Is used to establish connection between classess .
	 * Here, we have connected to 
	 * PasswordEncoder
	 * 
	 */
	@Autowired
	private PasswordEncoder encoder;
	
	/*
	 *  ADMIN REGISTER
	 *  value="/addAuthenticate" is the url for connecting angular in backend 
	 *  This url is used to insert the registered value from frontend to store into database. 
	 *  then the password is encoded for security purpose and then stored in database
	 *  
	 */
	@PostMapping("/ownerAuthenticate")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody LoginUser owner) throws Exception
	{
		List<OwnerLogin> ownerList=this.adminDetailService.getAllOwner();
		for(OwnerLogin ownerDetail:ownerList) {
			LoginUser loginOwner = new LoginUser();
			loginOwner.setEmail(ownerDetail.getEmail());
			loginOwner.setPassword(encoder.encode(ownerDetail.getPassword()));
			this.loginUserService.insertAdmin(loginOwner);
		}
		System.out.println("Owner authenticate ......");
		System.out.println(owner);
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(owner.getEmail(), owner.getPassword()));
		}
		catch(BadCredentialsException e)
		{
			System.out.println("login failed");
			return ResponseEntity.badRequest().body(new ResponsePage(Messages.FAILURE,"Login failed"));
		}
		return ResponseEntity.ok().body(new ResponsePage(Messages.SUCCESS,"Login success"));
	}
//CRUD ON OWNER
	
	@Autowired
	private OwnerCRUDService ownerservice;
	
	@GetMapping(value = "getAllOwnerDetails",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Owner> getAllOwners()
	{
	return ownerservice.getAllOwnerDetails();
	}
	@PostMapping(value = "storeOwnerDetails",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeOwnerInfo(@RequestBody Owner owner) 
	{	
	return ownerservice.storeOwnerDetails(owner);
	}
	
	@DeleteMapping(value = "deleteOwnerDetails/{Email}")
	public String deleteOwnerInfo(@PathVariable("Email") String Email)
	{
	return ownerservice.deleteOwnerDetails(Email);
	}
	
	@PatchMapping(value = "updateOwnerDetails")
	public String updateOwnerInfo(@RequestBody Owner owner) 
	{
	return ownerservice.updateOwnerDetails(owner);
	}
}

