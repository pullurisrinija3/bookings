package com.greatlearning.capstone.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.capstone.user.bean.User;
import com.greatlearning.capstone.user.service.UserCRUDService;

@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	UserCRUDService userservice;
	@GetMapping(value = "getAllUserDetails",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> getAllUsers()
	{
	return userservice.getAllUserDetails();
	}
	@PostMapping(value = "storeUserDetails",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeUserInfo(@RequestBody User user) 
	{	
	return userservice.storeUserDetails(user);
	}
	
	@DeleteMapping(value = "deleteUserDetails/{Email}")
	public String deleteUserInfo(@PathVariable("Email") String Email)
	{
	return userservice.deleteUserDetails(Email);
	}
	
	@PatchMapping(value = "updateUserDetails")
	public String updateUserInfo(@RequestBody User user) 
	{
	return userservice.updateUserDetails(user);
	}

}
