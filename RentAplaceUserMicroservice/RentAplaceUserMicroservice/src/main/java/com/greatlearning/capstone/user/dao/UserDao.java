package com.greatlearning.capstone.user.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.capstone.user.bean.User;

@Repository
public interface UserDao  extends JpaRepository<User, String>{

}
