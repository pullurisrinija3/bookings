package com.greatlearning.capstone.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.greatlearning.capstone.user.bean.User;
import com.greatlearning.capstone.user.dao.UserDao;

@Service
public class UserCRUDService {
	@Autowired
	UserDao userDao;
	public String storeUserDetails(User user)
	{	
		if(userDao.existsById(user.getEmail())) 
		{
		  return "Email must be unique";
		}else {
		  userDao.save(user);
		  return "User details stored successfully";
		}
    }
	
	public String deleteUserDetails(String Email)
	{
		if(!userDao.existsById(Email)) {
			return "No User present with the given email";
			}else {
			userDao.deleteById(Email);
			return "User deatils deleted successfully";
			}	
	}
		
	public String updateUserDetails(User user) {
		if(!userDao.existsById(user.getEmail())) 
		{
		    return "No User present with the given email";
		}
		else 
		{
			User u= userDao.getById(user.getEmail()); 
			u.setPassword(user.getPassword());					
			userDao.saveAndFlush(user);				
			return "User details updated successfully";
		}	
	}
	
	public List<User> getAllUserDetails(){
		return userDao.findAll();
	}
//	public String LogoutDetails(String Email)
//	{
//		if(!ownerDao.existsById(Email)) {
//			return "Owner is not present with the given email";
//			}else {
//			return "Owner logged out successfully";
//			}	
//	}


}
