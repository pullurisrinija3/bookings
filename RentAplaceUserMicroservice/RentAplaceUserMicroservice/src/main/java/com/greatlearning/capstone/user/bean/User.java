package com.greatlearning.capstone.user.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class User {
	@Id
	@Column(name="email")
	private String Email;
	private String password;
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User(String email, String password) {
		super();
		Email = email;
		this.password = password;
	}
	@Override
	public String toString() {
		return "User [Email=" + Email + ", password=" + password + "]";
	}
	

}
