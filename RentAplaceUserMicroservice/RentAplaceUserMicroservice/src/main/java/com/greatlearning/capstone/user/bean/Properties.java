package com.greatlearning.capstone.user.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Properties {
	@Id
	private String nameOfProperty;
	private String place;
	private String typeOfProperty;
	private String pool;
	private String beach;
	private String garden;
	private int totalRooms;
	private int rating;
	private float pricePerDay;
	public String getNameOfProperty() {
		return nameOfProperty;
	}
	public void setNameOfProperty(String nameOfProperty) {
		this.nameOfProperty = nameOfProperty;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getTypeOfProperty() {
		return typeOfProperty;
	}
	public void setTypeOfProperty(String typeOfProperty) {
		this.typeOfProperty = typeOfProperty;
	}
	public String getPool() {
		return pool;
	}
	public void setPool(String pool) {
		this.pool = pool;
	}
	public String getBeach() {
		return beach;
	}
	public void setBeach(String beach) {
		this.beach = beach;
	}
	public String getGarden() {
		return garden;
	}
	public void setGarden(String garden) {
		this.garden = garden;
	}
	public int getTotalRooms() {
		return totalRooms;
	}
	public void setTotalRooms(int totalRooms) {
		this.totalRooms = totalRooms;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public float getPricePerDay() {
		return pricePerDay;
	}
	public void setPricePerDay(float pricePerDay) {
		this.pricePerDay = pricePerDay;
	}
	public Properties(String nameOfProperty, String place, String typeOfProperty, String pool, String beach,
			String garden, int totalRooms, int rating, float pricePerDay) {
		super();
		this.nameOfProperty = nameOfProperty;
		this.place = place;
		this.typeOfProperty = typeOfProperty;
		this.pool = pool;
		this.beach = beach;
		this.garden = garden;
		this.totalRooms = totalRooms;
		this.rating = rating;
		this.pricePerDay = pricePerDay;
	}
	public Properties() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Properties [nameOfProperty=" + nameOfProperty + ", place=" + place + ", typeOfProperty="
				+ typeOfProperty + ", pool=" + pool + ", beach=" + beach + ", garden=" + garden + ", totalRooms="
				+ totalRooms + ", rating=" + rating + ", pricePerDay=" + pricePerDay + "]";
	}
	

}
