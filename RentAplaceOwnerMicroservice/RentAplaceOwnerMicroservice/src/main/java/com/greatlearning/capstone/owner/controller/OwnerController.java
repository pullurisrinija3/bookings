package com.greatlearning.capstone.owner.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.capstone.owner.bean.Owner;
import com.greatlearning.capstone.owner.service.OwnerService;

@RestController
@RequestMapping("/owner")
public class OwnerController {
	@Autowired
	OwnerService ownerservice;
	//CRUD operations on Owners
	@GetMapping(value = "getAllOwnerDetails",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Owner> getAllOwners()
	{
	return ownerservice.getAllOwnerDetails();
	}
	@PostMapping(value = "storeOwnerDetails",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeOwnerInfo(@RequestBody Owner owner) 
	{	
	return ownerservice.storeOwnerDetails(owner);
	}
	
	@DeleteMapping(value = "deleteOwnerDetails/{Email}")
	public String deleteOwnerInfo(@PathVariable("Email") String Email)
	{
	return ownerservice.deleteOwnerDetails(Email);
	}
	
	@PatchMapping(value = "updateOwnerDetails")
	public String updateOwnerInfo(@RequestBody Owner owner) 
	{
	return ownerservice.updateOwnerDetails(owner);
	}
}
//	//Owner Logout
//	@GetMapping(value = "Logout/{Email}")
//	public String OwnerLogoutInfo(@PathVariable("Email") String Email)
//	{
//	return ownerservice.LogoutDetails(Email);
//	}
//	}
	
