package com.greatlearning.capstone.owner.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.capstone.owner.bean.Owner;

@Repository
public interface OwnerDao extends JpaRepository<Owner,String> {

}
